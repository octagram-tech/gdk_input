pub mod input {
    extern crate keyboard_query;
    use keyboard_query::{DeviceQuery, DeviceState};
    use num_derive::FromPrimitive;
    use num_derive::ToPrimitive;
    use num_traits::ToPrimitive;
    use strum::IntoEnumIterator;
    use strum_macros::EnumIter;

    #[derive(Debug, Copy, Clone, ToPrimitive, FromPrimitive, EnumIter)]
    pub enum Key {
        Space,
        ExclamationMark,
        Quotations,
        Hashtag,
        DollarSign,
        PercentageSign,
        Ampersand,
        SingleComma,
        LeftBracket,
        RightBracket,
        Asterix,
        Plus,
        Comma,
        Dash,
        Fullstop,
        Slash,
        Key0,
        Key1,
        Key2,
        Key3,
        Key4,
        Key5,
        Key6,
        Key7,
        Key8,
        Key9,
        Colon,
        SemiColon,
        LeftThan,
        Equals,
        GreaterThan,
        QuestionMark,
        AtSymbol,
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        LeftSquareBracket,
        LeftSlash,
        RightSquareBracket,
        CapSymbol,
        Underscore,
        TemplateString,
        LkA,
        LkB,
        LkC,
        LkD,
        LkE,
        LkF,
        LkG,
        LkH,
        LkI,
        LkJ,
        LkK,
        LkL,
        LkM,
        LkN,
        LkO,
        LkP,
        LkQ,
        LkR,
        LkS,
        LkT,
        LkU,
        LkV,
        LkW,
        LkX,
        LkY,
        LkZ,
        LeftCurlyBracket,
        Pipe,
        RightCurlyBracket,
        CurlyDash,
        Delete,
    }

    static mut DEVICE_STATE: Option<DeviceState> = None;
    static mut LAST_KEY_PRESSED: Key = Key::Space;

    const ASCII_OFFSET: u16 = 32;

    pub fn get_device_state() -> &'static mut DeviceState {
        unsafe { DEVICE_STATE.get_or_insert(DeviceState::new()) }
    }

    pub fn key_pressed(keycode: Key) -> bool {
        let keys = get_device_state().get_keys();
        let code_in_cache = keys.contains(&(keycode.to_u16().unwrap() + ASCII_OFFSET));
        if code_in_cache {
            unsafe {
                LAST_KEY_PRESSED = keycode;
            }
        }
        code_in_cache
    }

    pub fn is_any_key_pressed() -> Option<Key> {
        for keycode in Key::iter() {
            if key_pressed(keycode) {
                return Some(keycode);
            }
        }

        None
    }

    pub fn get_last_key_pressed() -> Key {
        unsafe { LAST_KEY_PRESSED }
    }
}
